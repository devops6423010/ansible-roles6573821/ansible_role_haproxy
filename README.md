Haproxy install Ansible Role
=========

Role to install haproxy on target hosts.

Variables
----------------

### Socket and user settings
haproxy_socket: /var/lib/haproxy/stats
haproxy_chroot: /var/lib/haproxy
haproxy_user: haproxy
haproxy_group: haproxy

### Frontend settings
```
haproxy_frontend_name: 'hafrontend'
haproxy_frontend_bind_address: '*'
haproxy_frontend_port: 80
haproxy_frontend_mode: 'http'
```
### Backend settings
```
haproxy_backend_name: 'habackend'
haproxy_backend_mode: 'http'
haproxy_backend_balance_method: 'roundrobin'
haproxy_backend_httpchk: 'HEAD / HTTP/1.1\r\nHost:localhost'
```
### List of backend servers.
```
haproxy_backend_servers: 
 - name: app1
   address: 192.168.0.1
   port: 80
 - name: app2
   address: 192.168.0.2
   port: 80
```

### Extra global vars (see haproxy README for example usage).
```
haproxy_global_vars: []
```
### Default haproxy timeouts
```
haproxy_connect_timeout: 5000
haproxy_client_timeout: 50000
haproxy_server_timeout: 50000
```

Testing with Molecule
----------------
The default distribution for tests is ubuntu:22.04. You can override th with setting the environment variable MOLECULE_DISTRO
```
MOLECULE_DISTRO=debian:11 molecule test
```

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: sample.haproxy }

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov as a challege for ClassBook project https://deusops.com/classbook
* Based on geerlingguy.haproxy Ansible role https://github.com/geerlingguy/ansible-role-haproxy
